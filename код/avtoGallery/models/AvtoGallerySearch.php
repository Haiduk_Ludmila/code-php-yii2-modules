<?php

namespace backend\modules\avtoGallery\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\avtoGallery\models\AvtoGallery;

/**
 * AvtoGallerySearch represents the model behind the search form about `backend\modules\avtoGallery\models\AvtoGallery`.
 */
class AvtoGallerySearch extends AvtoGallery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'avto_id'], 'integer'],
            [['lang', 'title', 'subtitle', 'description_hover', 'title_left', 'description_left', 'title_right', 'description_right'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AvtoGallery::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'avto_id' => $this->avto_id,
        ]);

        $query->andFilterWhere(['like', 'lang', $this->lang])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'subtitle', $this->subtitle])
            ->andFilterWhere(['like', 'description_hover', $this->description_hover])
            ->andFilterWhere(['like', 'title_left', $this->title_left])
            ->andFilterWhere(['like', 'description_left', $this->description_left])
            ->andFilterWhere(['like', 'title_right', $this->title_right])
            ->andFilterWhere(['like', 'description_right', $this->description_right]);

        return $dataProvider;
    }
}
