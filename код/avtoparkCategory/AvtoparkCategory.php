<?php

namespace backend\modules\avtoparkCategory;

use Yii;

class AvtoparkCategory extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\avtoparkCategory\controllers';

    public function init()
    {
        parent::init();
		$theme = Yii::$app->params['theme'];
        $this->viewPath = '@app/themes/'.$theme.'/modules/'. $this->id;

        // custom initialization code goes here
    }
}
