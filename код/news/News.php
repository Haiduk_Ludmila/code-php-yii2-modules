<?php

namespace backend\modules\news;
use Yii;

class News extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\news\controllers';

    public function init()
    {
        parent::init();
		$theme = Yii::$app->params['theme'];
        $this->viewPath = '@app/themes/'.$theme.'/modules/'. $this->id ;

        // custom initialization code goes here
    }
}
